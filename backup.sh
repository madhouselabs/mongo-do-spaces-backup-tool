#! /bin/bash

echo "DB Backup script STARTED..."
echo "Taking DB backup"

cd /app

BACKUP_DIR=db-backup-$(date +%I_%M_%p-%d-%m-%Y)
mkdir $BACKUP_DIR

for db in $DB
do
    echo "Dumping [ $db ]"
    mongodump --uri "${MONGODB_URI%/}/$db" --out $BACKUP_DIR
done

echo "Zipping $BACKUP_DIR into $BACKUP_DIR.zip"

echo "Before Zipping, size: $( du -hs $BACKUP_DIR | awk '{print $1}' )"

zip $BACKUP_DIR.zip -r $BACKUP_DIR/* 1> /dev/null

echo "Zipping Complete, size: $( du -hs $BACKUP_DIR.zip | awk '{print $1}' ) "

echo "Now Pushing to Spaces ....."

python3 backup-tool.py $BACKUP_DIR.zip

status=$?;

echo "Backup script FINISHED..."

echo "[CleanUp]: Removing the directory and the zip"

[[ $status -eq 0 ]] && rm -rf $BACKUP_DIR $BACKUP_DIR.zip
