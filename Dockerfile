FROM alpine
RUN apk add bash mongodb-tools python3 py3-pip apk-cron zip tzdata
RUN pip install boto3

RUN echo "Asia/Kolkata" >> /etc/localtime

COPY ./crontab /etc/crontabs/root
RUN echo "" >> /etc/crontabs/root

RUN touch /var/log/cron.log

RUN mkdir /app
WORKDIR /app

COPY ./backup.sh ./
RUN chmod +x ./backup.sh

COPY ./backup-tool.py ./

# CMD cron && tail -f /var/log/cron.log
CMD crond -f -L /var/log/cron.log
# CMD ["crond", "-f"]
