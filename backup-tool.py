import sys
import os
import boto3
import hashlib
import base64

DEBUG = False

def getMD5Checksum(file_path):
    md5 = hashlib.md5()

    with open(file_path, "rb") as file:
        while chunk := file.read(4096):
            md5.update(chunk)

    return {"md5": '"' + md5.hexdigest() + '"', "base64": base64.b64encode(md5.digest()).decode("utf-8")}


class BackupTool:
    def __init__(self):
        self.session = boto3.session.Session()
        self.client = self.session.client(
            "s3",
            # region_name="ams3",
            region_name=os.environ["REGION_NAME"],
            endpoint_url=f"https://{os.environ['REGION_NAME']}.digitaloceanspaces.com",
            aws_secret_access_key=os.environ["SPACES_SECRET"],
            aws_access_key_id=os.environ["SPACES_KEY"],
        )

        self.DEFAULT_BUCKET = os.environ["BUCKET_NAME"]
        self.create_bucket(self.DEFAULT_BUCKET)

    def create_bucket(self, bucket_name):
        existing_buckets = set(
            [item["Name"] for item in self.client.list_buckets()["Buckets"]]
        )
        if bucket_name not in existing_buckets:
            self.client.create_bucket(Bucket=bucket_name)

    def upload(self, file_path, dir_prefix=None, retry_count=3):
        file_name = file_path.split("/")[-1]
        checksum = getMD5Checksum(file_path)

        if DEBUG:
            print(f"[debug] [ MD5 Checksum ]: {checksum}")

        with open(file_path, "rb") as file:
            resp = self.client.put_object(
                Bucket=self.DEFAULT_BUCKET,
                Key=f"{dir_prefix}/{file_name}" if dir_prefix else file_name,
                Body=file,
                ACL="private",
                ContentMD5=checksum["base64"],
            )

            if DEBUG:
                print("Response: ", resp, resp["ETag"])

            if checksum['md5'] == resp["ETag"]:
                print(" [MD5 Match Complete] File Upload Complete:", file_path)
            else:
                if retry_count > 0:
                    self.upload(file_path, dir_prefix, retry_count=retry_count - 1)
                else:
                    print(
                        "[FAILED, Retry Timeout]: File can't be uploaded due to mismatching MD5 checksum"
                    )
                    sys.exit(1);


if __name__ == "__main__":
    DIR = os.environ["DIR_PREFIX"]
    FILE_TO_UPLOAD = os.path.realpath(sys.argv[1])

    bkpTool = BackupTool()
    bkpTool.upload(FILE_TO_UPLOAD, DIR)
